# CLI Week

**My personal selection of apps for CLI week**

```
If you're a CLI or TUI beginner, I've put together this list of apps for you.
Everything on this list is easy to install, well documented, stable, and powerful.
They're almost always in your package manager, and some don't even need to be installed
to run. 
```


## Basic Desktop Apps

| Category        | App Name          | Git Repository                              | URL                                     | 
| --------------- | ----------------- | ------------------------------------------- | --------------------------------------- |
| File Manager    | **broot**         | https://github.com/Canop/broot              | https://dystroy.org/broot/              |
| File Manager    | **xplr**          | https://github.com/sayanarijit/xplr         | https://xplr.dev/                       |
| FIle Manager    | **yazi**          | https://github.com/sxyazi/yazi              | https://yazi-rs.github.io/              |
| Web Browser     | **carbonyl**      | https://github.com/fathyb/carbonyl          |                                         |
| Text Editor     | **micro**         | https://github.com/zyedidia/micro           | https://micro-editor.github.io/         |
| Calendar        | **caps-log**      | https://github.com/NikolaDucak/caps-log     |                                         |
| Passwords       | **kpxhs**         | https://github.com/akazukin5151/kpxhs       |                                         |
| Graphics        | **textual-paint** | https://github.com/1j01/textual-paint       | https://pypi.org/project/textual-paint/ |
| Music Player    | **musikcube**     | https://github.com/clangen/musikcube        | https://musikcube.com/                  |
| Radio Player    | **radio-active**  | https://github.com/deep5050/radio-active    | https://www.radio-browser.info          |
| Online Video    | **youtube-tui**   | https://siriusmart.github.io/youtube-tui/   |                                         |
| Online Video    | **magic-tape**    | https://gitlab.com/christosangel/magic-tape |                                         |
| YouTube         | **ytfzf**         | https://github.com/pystardust/ytfzf         |                                         |
| Markdown Viewer | **frogmouth**     | https://github.com/Textualize/frogmouth     |                                         |
| Weather         | **wttr.in**       | https://github.com/chubin/wttr.in           | https://wttr.in/                        |
| Spotify         | **ncspot**        | https://github.com/hrkfdn/ncspot            |                                         |
| Multiplexer     | **zellij**        | https://github.com/zellij-org/zellij        | https://zellij.dev/                     |
| Screen Recorder | **asciinema**     | https://github.com/asciinema/asciinema      | https://asciinema.org/                  |


## System

| Category        | App Name        | Git Repository                                                   | URL                             | 
| --------------- | --------------- | ---------------------------------------------------------------- | ------------------------------- |
| Networking      | **nmtui**       | https://developer-old.gnome.org/NetworkManager/stable/nmtui.html |            |
| Networking      | **trippy**      | https://github.com/fujiapple852/trippy                           | https://trippy.cli.rs/          |
| Bluetooth       | **bluetuith**   | https://github.com/darkhz/bluetuith                              |                                 |
| Bluetooth       | **bluetui**     | https://github.com/pythops/bluetui                               |                                 |
| Brightness      | **rumos**       | https://github.com/octagony/rumos                                |                                 |
| Sound           | **pulsemixer**  | https://github.com/GeorgeFilipkin/pulsemixer                     |                                 |
| Processes       | **btop**        | https://github.com/aristocratos/btop                             |                                 |
| Monitoring      | **whowatch**    | https://www.tecmint.com/whowatch-monitor-linux-users-and-processes-in-real-time/ |                 |


## Development Apps

| Category        | App Name      | Git Repository                           | URL                             | 
| --------------- | ------------- | ---------------------------------------- | ------------------------------- |
| Code IDE        | **helix**     | https://github.com/helix-editor/helix    | https://helix-editor.com/       |
| SQL             | **lazysql**   | https://github.com/jorgerojas26/lazysql  |                                 |
| API Testing     | **httpie**    | https://github.com/httpie/cli            | https://httpie.io/              |
| Git             | **lazygit**   | https://github.com/jesseduffield/lazygit |                                 |
| Git             | **gitui**     | https://github.com/extrawurst/gitui      |                                 |
| Open AI         | **elia**      | https://github.com/darrenburns/elia      |                                 |
| Kanban Board    | **kanban**    | https://github.com/Zaloog/kanban-python  |                                 |
ex

## Games (a sampling)

| Category        | App Name      | Git Repository                               | URL                             | 
| --------------- | ------------- | -------------------------------------------- | ------------------------------- |
| Math            | **Sudoku**    | https://gitlab.com/christosangel/tui-sudoku  |                                 |
| Spelling        | **Hangman**   | https://github.com/braheezy/hangman          |                                 |
| Spelling        | **clide**     | https://github.com/ajeetdsouza/clidle        |                                 |
| Arcade          | **Doom**      | https://github.com/wojciech-graj/doom-ascii  |                                 |
